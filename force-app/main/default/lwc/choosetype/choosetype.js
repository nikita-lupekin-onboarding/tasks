
import { LightningElement, track, wire} from 'lwc';
import { getPicklistValues } from 'lightning/uiObjectInfoApi';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import PRODUCT_OBJECT from '@salesforce/schema/Product2';
import Type_FIELD from '@salesforce/schema/Product2.Family';

export default class Choosetype extends LightningElement {
    @track value;

    @wire(getObjectInfo, { objectApiName: PRODUCT_OBJECT })
    objectInfo;

    @wire(getPicklistValues, { recordTypeId: '0124x0000004p2HAAQ', fieldApiName: Type_FIELD})
    TypePicklistValues;

    //@wire(getPicklistValues, { recordTypeId: '$objectInfo.data.defaultRecordTypeId', fieldApiName: INDUSTRY_FIELD})
    //IndPicklistValues;

    handleChange(event) {
        this.value = event.detail.value;
    }
}
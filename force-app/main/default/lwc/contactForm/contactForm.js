import { LightningElement,api } from 'lwc';
import CONTACT_OBJECT from '@salesforce/schema/Contact';
import CONTACT_NAME_FIELD from '@salesforce/schema/Contact.Name';
import CONTACT_LASTNAME_FIELD from '@salesforce/schema/Contact.LastName';
import ACC_CONTACT from '@salesforce/schema/Contact.AccountId';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class ContactForm extends LightningElement{
    @api contactid;

    clickedButtonLabel;
    message;
    objectApiName = CONTACT_OBJECT;
    handleSuccess(event){
        const toastEvent = new ShowToastEvent({
            title: "Contact created",
            message: "Record ID: " + event.detail.id,
            variant: "success"
        });
        this.dispatchEvent(toastEvent);
    }
    @api justsetaccount(AccountId){
        console.log("Getting text: "+AccountId);
        this.contactid=AccountId;
        this.ACC_CONTACT=this.contactid;
    }
    handleClick(event){
        this.contactid;
        this.clickedButtonLabel=this.contactid;
    }
}